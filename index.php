<?php
require("layouts/head.php");

?>

<div class="d-flex justify-content-between">
	<h1>Products</h1>
		<div class="pt-3">
			<button id="addBtn" class="btn btn-primary">ADD</button>
			<button id="deleteBtn" class="btn btn-danger">MASS DELETE</button>
		</div>
</div>
<div class="d-flex flex-wrap pt-3" id="content">
	<p>Nothing</p>
</div>

<script src="js/index.js"></script>

<?php
	require("layouts/footer.html");
?>