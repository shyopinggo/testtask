function setSpecificAttributeField() {
	let productSwitcher = document.getElementById('productType');
	let attributeMessage = document.getElementById('attributeMessage');
	let attributeField = document.getElementById('attributeField')
	if (productSwitcher.value == 'DVD') {
		attributeMessage.innerHTML = 'Please, provide size (in MB)';
		let html = '<div class="form-group form-row">';
		html += '<div class="col">';
		html += '<label for="size">Size:</label></div>';
		html += '<div class="col">';
		html += '<input type="text" id="size" name="size" class="form-control"></div>';
		html += '</div>';
		attributeField.innerHTML = html;
	}
	else if (productSwitcher.value == 'Book') {
		attributeMessage.innerHTML = 'Please, provide weight (in kg)';
		let html = '<div class="form-group form-row">';
		html += '<div class="col">';
		html += '<label for="weight">Weight:</label></div>';
		html += '<div class="col">';
		html += '<input type="text" id="weight" name="weight" class="form-control" ></div>';
		html += '</div>';
		attributeField.innerHTML = html;
	}
	else if (productSwitcher.value == 'Furniture') {
		attributeMessage.innerHTML = 'Please, provide dimensions';
		let html = '<div class="form-group form-row">';
		html += '<div class="col">';
		html += '<label for="height">Height:</label></div>';
		html += '<div class="col">';
		html += '<input type="text" id="height" name="height" class="form-control" ></div>';
		html += '</div>';
		html += '<div class="form-group form-row">';
		html += '<div class="col">';
		html += '<label for="width">Width:</label></div>';
		html += '<div class="col">';
		html += '<input type="text" id="width" name="width" class="form-control" ></div>';
		html += '</div>';
		html += '<div class="form-group form-row">';
		html += '<div class="col">';
		html += '<label for="length">length:</label></div>';
		html += '<div class="col">';
		html += '<input type="text" id="length" name="length" class="form-control" ></div>';
		html += '</div>';
		attributeField.innerHTML = html;
	}
}

document.getElementById('productType').addEventListener('change', setSpecificAttributeField);

setSpecificAttributeField();

document.getElementById('cancelBtn').addEventListener('click', () => {
	event.preventDefault();
	document.location = 'index.php';
});

function sendForm() {
	event.preventDefault();
	let sku = document.getElementById('sku').value;
	let name = document.getElementById('name').value;
	let price = document.getElementById('price').value;
	let productType = document.getElementById('productType').value;
	let size;
	let weight;
	let width;
	let length;
	let height;
	if (document.getElementById('size')) size = document.getElementById('size').value;;
	if (document.getElementById('weight')) weight = document.getElementById('weight').value;;
	if (document.getElementById('height')) height = document.getElementById('height').value;;
	if (document.getElementById('width')) width = document.getElementById('width').value;;
	if (document.getElementById('length')) length = document.getElementById('length').value;;
	let additional_attribute = new Array();
	if (size) additional_attribute.push(size);
	if (weight) additional_attribute.push(weight);
	if (height) additional_attribute.push(height);
	if (width) additional_attribute.push(width);
	if (length) additional_attribute.push(length);
	let data = {
		sku: sku,
		name: name,
		price: price,
		product_type: productType,
		specific_attribute: additional_attribute
	};
	fetch('/api/add.php', {method: 'POST', body: JSON.stringify(data)})
	.then(response => {
		if (response.status == 200) {
			document.location = "index.php";
		}
		else if (response.status == 400) {
			return response.json();
		}
	})
.then(data => {document.getElementById('errorMessage').innerHTML = data.error;})
	.catch(error => console.error(error));
}

document.getElementById('saveBtn').addEventListener('click', sendForm);