document.getElementById('addBtn').addEventListener('click', () => {
	document.location = 'add-product.php';
});

document.getElementById('deleteBtn').addEventListener('click', () => {
	let data = new Array();
	let elements = document.getElementsByClassName('delete-checkbox');
	if (elements.length > 0)
	{
		for (let i = 0; i < elements.length; i++)
		{
			if (elements[i].checked) {
				data.push(elements[i].getAttribute("value"));
			}
		}
		let body = {data: data};
		fetch('api/massdelete.php', {method: 'POST', body: JSON.stringify(body)})
	.then(response => {
		if (response.status == 200) {
			document.location = 'index.php'
		}
	})
	.catch(error => console.error(error));
	}
});

fetch('api/getall.php')
.then(response => response.json())
.then(data => {
	let html = '';
	data.forEach(product => {
		html += '<div class="product">';
		html += '<input type="checkbox" name="products[]" value="' + product.id + '" class="delete-checkbox">';
		html += '<p>';
		html += 'SKU: ' + product.sku + '<br>';
		html += 'Name: ' + product.name + '<br>';
		html += 'Price: $' + product.price + '<br>';
		html += product.specific_attribute_name + ': ' + product.specific_attribute_value + '<br>';
		html += '</p>';
		html += '</div>';
	});
	document.getElementById('content').innerHTML = html;
})
.catch(error => console.error(error));