<?php
if ($_SERVER["REQUEST_METHOD"] != "POST") exit();

include_once("../model/database.php");
include_once("../model/productfactory.php");

header("Content-Type: application/json; charset=UTF-8");

$data = json_decode(file_get_contents("php://input"));

$factory = new ProductFactory();
$product = $factory->create($data->product_type);
try
{
	$product->setSku($data->sku);
	$product->setName($data->name);
	$product->setPrice($data->price);
	$product->setSpecificAttributeValue($data->specific_attribute);
}
catch (Exception $ex)
{
	http_response_code(400);
	echo json_encode(array("error" => "Please, enter required data in correct format.."));
	exit();
}
$db = new Database();
$result = $product->saveProduct($db->getConnection());
if ($result == 1)
{
	http_response_code(200);
	echo json_encode(array("message" => "ok"));
}
else
{
	http_response_code(400);
	echo json_encode(array("error" => "Failed to add a product to the database. Probably the product with given SKU already exists."));
}
?>