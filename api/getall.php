<?php
include_once("../model/database.php");
include_once("../model/product.php");

header("Content-Type: application/json; charset=UTF-8");

$db = new Database();
$conn = $db->getConnection();
$products = Product::getAllProducts($conn);
if (count($products) > 0)
{
	$result = [];
	foreach ($products as $product)
	{
		$data = [];
		$data["id"] = $product->getId();
		$data["sku"] = $product->getSku();
		$data["name"] = $product->getName();
		$data["price"] = $product->getPrice();
		$data["specific_attribute_name"] = $product->getSpecificAttributeName();
		$data["specific_attribute_value"] = $product->getSpecificAttributeValue();
		$result[] = $data;
	}
	http_response_code(200);
	echo json_encode($result);
}
else
{
	http_response_code(404);
	echo json_encode(array("error" => "Nothing was found"));
}
?>