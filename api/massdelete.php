<?php
if ($_SERVER["REQUEST_METHOD"] != "POST") exit();

include_once("../model/database.php");
include_once("../model/product.php");

header("Content-Type: application/json; charset=UTF-8");

$data = json_decode(file_get_contents("php://input"));
try
{
	$db = new Database();
$connection = $db->getConnection();
foreach($data->data as $id)
{
	Product::deleteProductById($id, $connection);
}
http_response_code(200);
}
catch (Exception $ex)
{
	http_response_code(500);
	echo json_encode(array("error" => $ex->getMessage()));
}

?>