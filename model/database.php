<?php
include_once("productfactory.php");

class Database
{
	private $hostName = "localhost";
	private $dbName = "test";
	private $userName = "test";
	private $password = "Tt987848@";
	private $connection;
	
	public function __construct()
	{
		$this->connection = new PDO("mysql:host=$this->hostName;dbname=$this->dbName", $this->userName, $this->password);
	}
	
	public function getConnection()
	{
		return $this->connection;
	}
}
?>