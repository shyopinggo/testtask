<?php
abstract class Product
{
	protected $id;
	protected $sku;
	protected $name;
	protected $price;
	protected $specificAttributeName;
	protected $specificAttributeValue;
	protected $productType;
	
	public function getId()
	{
		return $this->id;
	}
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function getSku()
	{
		return $this->sku;
	}
	
	public function setSku($sku)
	{
		$this->validate($sku, false);
		$this->sku = $sku;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function setName($name)
	{
		$this->validate($name, false);
		$this->name = $name;
	}
	
	public function getPrice()
	{
		return $this->price;
	}
	
	public function setPrice($price)
	{
		$this->validate($price, true);
		$this->price = $price;
	}
	
	public function getSpecificAttributeName()
	{
		return $this->specificAttributeName;
	}
	
	public function getSpecificAttributeValue()
	{
		return $this->specificAttributeValue;
	}
	
	public abstract function setSpecificAttributeValue($values);
	
	public function getProductType()
	{
		return $this->productType;
	}
	
	public function saveProduct($connection)
	{
		$sql = "INSERT INTO products (sku, name, price, product_type, specific_attribute) VALUES (:sku, :name, :price, :product_type, :specific_attribute)";
		$query = $connection->prepare($sql);
		$query->bindValue(":sku", $this->getSku());
		$query->bindValue(":name", $this->getName());
		$query->bindValue(":price", $this->getPrice());
		$query->bindValue(":product_type", $this->getProductType());
		$query->bindValue(":specific_attribute", $this->getSpecificAttributeValue());
		$result = $query->execute();
		return $result;
	}
	
	public static function deleteProductById($id, $connection)
	{
		$sql = "DELETE FROM products WHERE id=:id";
		$query = $connection->prepare($sql);
		$query->bindValue(":id", $id);
		$query->execute();
	}
	
	public static function getAllProducts($connection)
	{
		$sql = "SELECT * FROM products";
		$data = $connection->query($sql);
		$result = [];
		$factory = new ProductFactory();
		foreach ($data as $row)
		{
			$product = $factory->create($row["product_type"]);
			$product->setId($row["id"]);
			$product->setSku($row["sku"]);
			$product->setName($row["name"]);
			$product->setPrice($row["price"]);
			$product->setSpecificAttributeValue($row["specific_attribute"]);
			$result[] = $product;
		}
		return $result;
	}
	
	protected function validate($value, $checkNumeric)
	{
		$isValid = true;
		if (empty($value))
		{
			$isValid = false;
		}
		if ($checkNumeric)
		{
			if (!is_numeric($value))
			{
				$isValid = false;
			}
			else if ($value < 0)
			{
				$isValid = false;
			}
		}
		if (!$isValid) throw new Exception("Invalid data");
	}
}
?>