<?php
include_once("product.php");

class Book extends Product
{
	public function __construct()
	{
		$this->specificAttributeName = "Weight";
		$this->productType = "Book";
	}
	
	public function setSpecificAttributeValue($values)
	{
		$value;
		if (gettype($values) == "array")
		{
			$value = $values[0];
		}
		else
		{
			$value = $values;
		}
		if (is_numeric($value))
		{
			$this->validate($value, true);
			$this->specificAttributeValue = "$value kg";
		}
		else
		{
			$this->validate($value, false);
			$this->specificAttributeValue = $value;
		}
	}
}
?>