<?php
include_once("product.php");

class DVD extends Product
{
	public function __construct()
	{
		$this->specificAttributeName = "Size";
		$this->productType = "DVD";
	}
	
	public function setSpecificAttributeValue($values)
	{
		$value;
		if (gettype($values) == "array")
		{
			$value = $values[0];
		}
		else
		{
			$value = $values;
		}
		if (is_numeric($value))
		{
			$this->validate($value, true);
			$this->specificAttributeValue = "$value MB";
		}
		else
		{
			$this->validate($value, false);
			$this->specificAttributeValue = $value;
		}
	}
}
?>