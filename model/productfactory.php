<?php
include_once("book.php");
include_once("dvd.php");
include_once("furniture.php");

class ProductFactory
{
	private $creaters;
	
	public function __construct()
	{
		$this->creaters = [];
		$this->creaters["Book"] = function() { return new Book(); };
		$this->creaters["DVD"] = function() { return new DVD(); };
		$this->creaters["Furniture"] = function() { return new Furniture(); };
	}
	
	public function create($type)
	{
		return $this->creaters[$type]();
	}
}
?>