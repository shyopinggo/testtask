<?php
include_once("product.php");

class Furniture extends Product
{
	public function __construct()
	{
		$this->specificAttributeName = "Dimensions";
		$this->productType = "Furniture";
	}
	
	public function setSpecificAttributeValue($values)
	{
		if (count($values) != 3)
		{
			$this->validate($values, false);
			$this->specificAttributeValue = $values;
		}
		else
		{
			$this->validate($values[0], true);
			$this->validate($values[1], true);
			$this->validate($values[2], true);
			$this->specificAttributeValue = $values[0]."x".$values[1]."x".$values[2];
		}
	}
}
?>