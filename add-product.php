<?php
	require("layouts/head.php");
	
?>

<h1>Add a product</h1>
<form id="product_form" class="col-sm-6" method="POST" action="add-product.php">
	<p id="errorMessage"></p>
	<div class="form-group form-row">
		<div class="col">
			<label for="sku">SKU:</label>
		</div>
		<div class="col">
			<input type="text" id="sku" name="sku" class="form-control">
		</div>
	</div>
	<div class="form-group form-row">
		<div class="col">
			<label for="name">Name:</label>
		</div>
		<div class="col">
			<input type="text" id="name" name="name" class="form-control">
		</div>
	</div>
	<div class="form-group form-row">
		<div class="col">
			<label for="price">price:</label>
		</div>
		<div class="col">
			<input type="text" id="price" name="price" class="form-control" >
		</div>
	</div>
	<div class="form-group form-row">
		<div class="col">
			<label for="productType">Product type:</label>
		</div>
		<div class="col">
			<select id="productType" name="productType" class="form-control">
				<option value="DVD">DVD</option>
				<option value="Book">Book</option>
				<option value="Furniture">Furniture</option>
			</select>
		</div>
	</div>
	<p id="attributeMessage"></p>
	<div id="attributeField">
	</div>
	<div class="form-group">
	<button id="cancelBtn" class="btn btn-danger">Cancel</button>
	<input id="saveBtn" class="btn btn-primary" type="submit" value="Save">
	</div>
</form>

<script src="js/add-product.js"></script>
<?php
	require("layouts/footer.html");
?>